package ito.poo.app;

import java.time.LocalDate;
import ito.poo.clases.Empaquetado;
import ito.poo.clases.Llenado;
import ito.poo.clases.Lavado;

public class Myapp {

	public  static  void  run () {
		Lavado lavado =  new Lavado ( " Maquina de lavado " , LocalDate.of( 2012 , 02 , 27 ), 15000F , 150F , 20 );
		Llenado llenado = new Llenado ( " Maquina de llenado " , LocalDate.of( 2011 , 01 , 23 ), 5000F , 15 , " 100ml, 500ml " );
		Empaquetado empaquetado = new Empaquetado ( " Maquina de empaquetado " , LocalDate.of( 2014 , 06 , 15 ), 8500F , 9 , 3 );
		System.out.println (lavado);
		System.out.println ( " Costo de lavado por botella: " + lavado . costoLavado () + " $ " );
		System.out.println ();
		System.out.println (llenado);
		System.out.println ( " Costo de llenado por botella: " + llenado . costoLlenado () + " $ " );
		System.out.println ();
		System.out.println (empaquetado);
		System.out.println ( " Costo de empaquetado por botella: " + empaquetado . costoEmpaquetado () + " $ " );
	}
	public  static  void  main ( String [] Args ) {
		run();
	}
}
