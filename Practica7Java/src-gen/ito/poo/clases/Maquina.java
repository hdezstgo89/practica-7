package ito.poo.clases;

import java.time.LocalDate;

public class Maquina {

	private String descripcion =  " ";
	private LocalDate fechaAdquisicion = null;
	private float costo = 0F;

	public  Maquina () {
		super ();
	}
	
	public Maquina(String descripcion, LocalDate fechaAdquisicion, float costo) {
		super();
		this.descripcion = descripcion;
		this.fechaAdquisicion = fechaAdquisicion;
		this.costo = costo;
	}

	public String getDescripcion () {
		return descripcion;
	}

	public void setDescripcion (String newDescripcion) {
		this.descripcion = newDescripcion;
	}

	public LocalDate getFechaAdquisicion () {
		return fechaAdquisicion;
	}

	public void setFechaAdquisicion (LocalDate newFechaAdquisicion) {
		this.fechaAdquisicion = newFechaAdquisicion;
	}

	public  float  getCosto () {
		return costo;
	}

	public void setCosto (float  newCosto) {
		this.costo = newCosto;
	}

	@Override
	public String toString() {
		return "Maquina [descripcion=" + descripcion + ", fechaAdquisicion=" + fechaAdquisicion + ", costo=" + costo + "]";
	}
}
