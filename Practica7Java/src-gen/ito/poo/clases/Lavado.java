package ito.poo.clases;

import java.time.LocalDate;

public class Lavado extends Maquina {

	private float capacidadLitros = 0F;
	private int tiempoCicloLavado = 0;

	public Lavado(String descripcion, LocalDate fechaAdquisicion, float costo, float capacidadLitros, int tiempoCicloLavado) {
		super(descripcion, fechaAdquisicion, costo);
		this.capacidadLitros = capacidadLitros;
		this.tiempoCicloLavado = tiempoCicloLavado;
	}

	public float costoLavado() {
		return (( .05F * this.getCosto ()) / 100) / ( 60 / this.tiempoCicloLavado);
	}

	public float getCapacidadLitros() {
		return capacidadLitros;
	}

	public void setCapacidadLitros(float newcapacidadLitros) {
		this.capacidadLitros = newcapacidadLitros;
	}

	public int getTiempoCicloLavado() {
		return tiempoCicloLavado;
	}

	public void setTiempoCicloLavado(int newtiempoCicloLavado) {
		this.tiempoCicloLavado = newtiempoCicloLavado;
	}

	@Override
	public String toString() {
		return "Lavado [capacidadLitros = " + capacidadLitros + ", tiempoCicloLavado = " + tiempoCicloLavado + "  " + super.toString () +  "]";
	}
}
