package ito.poo.clases;

import java.time.LocalDate;

public class Llenado extends Maquina{

	private int envasesXminuto = 0;
	private String regulacionMililitros = " ";

	public Llenado(String descripcion, LocalDate fechaAdquisicion, float costo, int envasesXminuto, String regulacionMililitros) {
		super(descripcion, fechaAdquisicion, costo);
		this.envasesXminuto = envasesXminuto;
		this.regulacionMililitros = regulacionMililitros;
	}
	
	public float costoLlenado () {
		return (( .25F * this.getCosto ()) / 100) / this.envasesXminuto;
	}

	public int getEnvasesXminuto() {
		return envasesXminuto;
	}

	public void setEnvasesXminuto(int newenvasesXminuto) {
		this.envasesXminuto = newenvasesXminuto;
	}

	public String getRegulacionMililitros() {
		return regulacionMililitros;
	}

	public void setRegulacionMililitros(String newregulacionMililitros) {
		this.regulacionMililitros = newregulacionMililitros;
	}

	@Override
	public String toString() {
		return "Llenado y envasado [envasesXminuto=" + envasesXminuto + ", regulacionMililitros=" + regulacionMililitros + " " +  super.toString () + "]";
	}
}
