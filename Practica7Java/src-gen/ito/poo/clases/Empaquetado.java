package ito.poo.clases;

import java.time.LocalDate;

public class Empaquetado extends Maquina{
	
	private int tipoEmpaque = 0;
	private int cantidadXminuto = 0;

	public Empaquetado(String descripcion, LocalDate fechaAdquisicion, float costo, int tipoEmpaque, int cantidadXminuto) {
		super(descripcion, fechaAdquisicion, costo);
		this.tipoEmpaque = tipoEmpaque;
		this.cantidadXminuto = cantidadXminuto;
	}
	
	public float costoEmpaquetado () {
		return (( .06F * this.getCosto ()) / 100 ) / ( this.cantidadXminuto * this.tipoEmpaque);
	}

	public int getTipoEmpaque() {
		return tipoEmpaque;
	}

	public void setTipoEmpaque(int newtipoEmpaque) {
		this.tipoEmpaque = newtipoEmpaque;
	}

	public int getCantidadXminuto() {
		return cantidadXminuto;
	}

	public void setCantidadXminuto(int newcantidadXminuto) {
		this.cantidadXminuto = newcantidadXminuto;
	}

	@Override
	public String toString() {
		return "Empaquetado [tipoEmpaque=" + tipoEmpaque + ", cantidadXminuto=" + cantidadXminuto + " " + super . toString () + " ]";
	}
}
